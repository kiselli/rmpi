## RmPi
## Dependencies
#### Mac OSX X & Homebrew
``brew install python #installs python2.7``
``brew install pygame #installs music player (native on raspian)``
``brew install open-mpi #mpi core library``
``pip install mpi4py #mpi python lib``

## Run

``mpirun -np 2 python rmpi ## -np is number processors``