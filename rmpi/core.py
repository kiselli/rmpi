import pygame
import time
from mpi4py import MPI

SEG_FILE = 'segments.txt'
SEG_DIR = 'segments'


def start():

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    print rank
    SEGMENTS = init_segments()
    print len(SEGMENTS)

    data = {'index': 0}
    while True:
        if rank == 0:
            comm.send(data, dest=1, tag=11)

            data = comm.recv(source=1, tag=11)
            print "main"
            print data
            play_segment(SEGMENTS,data['index'])
            #time.sleep(1)
            data['index'] = (data['index']+1) % len(SEGMENTS)

        elif rank == 1:
            data = comm.recv(source=0, tag=11)
            print "slave"
            print data
            #time.sleep(1)
            play_segment(SEGMENTS,data['index'])
            data['index'] = (data['index']+1) % len(SEGMENTS)
            comm.send(data, dest=0, tag=11)




def play():
    pygame.mixer.init()
    pygame.mixer.music.load("song.wav")
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy() == True:
        continue

def play_segments():
    pygame.mixer.init()
    lines = [line.strip() for line in open(SEG_FILE)]
    for i in range(0, len(lines)):
        play_segment(i, lines)
        while pygame.mixer.music.get_busy() == True:
            continue

def init_segments():
    pygame.mixer.init()
    return [line.strip() for line in open(SEG_FILE)]

def play_segment(SEGMENTS,nr):
    segment = SEGMENTS[nr]
    pygame.mixer.music.load(SEG_DIR+'/{0}'.format(segment))
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy() == True:
        continue
